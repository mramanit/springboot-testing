package mg.mamisoa.formunitteest.sbtest.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


@WebMvcTest(FirstController.class)
public class FirstControllerTest
{

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testFirstController()
        throws Exception
    {
        RequestBuilder req = MockMvcRequestBuilders
                                    .get("/first-action")
                                    .accept(MediaType.APPLICATION_JSON);
        MvcResult resp = mockMvc.perform(req).andReturn();
        assertEquals("First action", resp.getResponse().getContentAsString());
    }

}
